import React, { Component } from 'react';
import { Slide, Fade, Zoom } from 'react-slideshow-image';

import logo from './logo.svg';
import './App.css';

import slide_1 from './images/slide_1.jpg';
import slide_2 from './images/slide_2.jpg';
import slide_3 from './images/slide_3.jpg';
import slide_4 from './images/slide_4.jpg';
import slide_5 from './images/slide_5.jpg';


class App extends Component {

  render() {

    const properties = {
      duration: 5000,
      transitionDuration: 500,
      infinite: true,
      indicators: true,
      arrows: true
    }

    const fadeProperties = {
      duration: 5000,
      transitionDuration: 500,
      infinite: false,
      indicators: true
    }

    const zoomOutProperties = {
      duration: 5000,
      transitionDuration: 500,
      infinite: true,
      indicators: true,
      scale: 0.4,
      arrows: true
    }

    const slides = [
      slide_1,
      slide_2,
      slide_3,
      slide_4,
      slide_5
    ];

    const Slideshow = () => {
      return (
        <Slide {...properties}>
          {
            slides.map((slide, i) => (
              <div className="each-slide">
                <div style={{ 'backgroundImage': `url(${slide})` }}>
                  <span>Slide {i}</span>
                </div>
              </div>
            ))
          }
        </Slide>
      )
    }


    const fadedSlides = () => {
      return (
        <Fade {...fadeProperties}>
          {
            slides.map((slide, i) => (
              <div className="each-fade">
                <div className="image-container">
                  <img src={slide} />
                </div>
              </div>
            ))
          }
        </Fade>
      )
    }


    const zoomSlides = () => {
      return (
        <Zoom {...zoomOutProperties}>
          {
            slides.map((each, index) => <img key={index} style={{ width: "100%" }} src={each} />)
          }
        </Zoom>
      )
    }

    return (
      <div className="App">
        <div class="custom-slides-container">
          <h2>Faded:</h2>
          {fadedSlides()}
        </div>
        <div class="custom-slides-container">
          <h2>ZoomOut:</h2>
          {zoomSlides()}
        </div>
        <div class="custom-slides-container">
          <h2>Normal:</h2>
          {Slideshow()}
        </div>
      </div>
    );
  }
}

export default App;
